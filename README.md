# Flatscher Samuel
*Repository f�r SWP-SEPM und SWP-OP 2017/18*

### Projekte:

| Projektname | Mitarbeiter | Beschreibung | Status |
| ------ | ------ | ------ | ------ |
| Kunde | X | Markdown-HTML-Converter | abgeschlossen |
| Sortieralgorithmus| X | Bubblesort, Insertionsort mit MyLinkedList, Quicksort | in Arbeit |
| Rekursives Programmieren | X | Fakult�t, isPalindrom, Pascalsches Dreieck, binarySearch | in Arbeit |
| Insertionsort in FSharp | X | rekursiv aufgebauter Insertionsort in FSharp | abgeschlossen |

### ToDos
