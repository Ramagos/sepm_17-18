import java.awt.Component;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

public class MyLittleZoo {
	public static ArrayList<Tier> alleTiere=new ArrayList<Tier>();
	public static JFrame fenster;

	public static void main(String[] args) {
		try {
			fenster=new View();
			loadZoo();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void loadZoo() {
		try {
			File file=new File("saves/zoo.ser");
			if(file.exists()) {
				InputStream file1 = new FileInputStream("saves/zoo.ser");
				InputStream buffer = new BufferedInputStream(file1);
				ObjectInput input = new ObjectInputStream (buffer);
				alleTiere=(ArrayList<Tier>)input.readObject();
			}
			View.tiereAnzeigen();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public static void neuesTier(ArrayList<JRadioButton> radioButton, JTextField tierField) {
		String typ="";
		String name=tierField.getText();;

		if(radioButton.get(0).isSelected())
			typ="Kuh";
		else
			if(radioButton.get(1).isSelected())
				typ="Schaf";
			else
				if(radioButton.get(2).isSelected())
					typ="Katze";
				else
					if(radioButton.get(3).isSelected())
						typ="Hund";

		switch(typ) {
		case "Kuh": {
			Tier neu=new Kuh(name);
			alleTiere.add(neu);
		} break;
		case "Schaf": {
			Tier neu=new Schaf(name);
			alleTiere.add(neu);
		} break;
		case "Katze": {
			Tier neu=new Katze(name);
			alleTiere.add(neu);
		} break;
		case "Hund": {
			Tier neu=new Hund(name);
			alleTiere.add(neu);
		} break;
		}
		datenAktualisieren();
	}
	public static void tiereFreilassen(ArrayList<JCheckBox> checkBoxes) {
		int removed=0;
		for(int i=0;i<checkBoxes.size();i++) {
			if(checkBoxes.get(i).isSelected()) {
				alleTiere.remove(i-removed);
				removed++;
			}
		}
		datenAktualisieren();
	}
	public static void datenAktualisieren() {
		try {
			File file=new File("saves/zoo.ser");
			file.delete();
			FileOutputStream fileOut = new FileOutputStream("saves/zoo.ser");
			ObjectOutputStream out = new ObjectOutputStream(fileOut);
			out.writeObject(alleTiere);
			out.close();
			fileOut.close();
			View.tiereAnzeigen();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}