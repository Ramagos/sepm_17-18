import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.swing.*;
import javax.swing.border.*;


public class View extends JFrame{
	public static JPanel kuehe;
	public static JPanel schafe;
	public static JPanel katzen;
	public static JPanel hunde;

	public static JTextArea ausgabe;

	public View() {
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setTitle("MyLittleZoo");
		this.setSize(800,500);
		this.setLayout(new BorderLayout());
		this.setVisible(true);


		JPanel headings=new JPanel();

		headings.setPreferredSize(new Dimension(800,25));
		headings.setLayout(new GridLayout(0,4));
		headings.setBackground(Color.LIGHT_GRAY);

		JLabel kueheText=new JLabel("Kühe", SwingConstants.CENTER);
		headings.add(kueheText);

		JLabel schafeText=new JLabel("Schafe", SwingConstants.CENTER);
		headings.add(schafeText);

		JLabel katzenText=new JLabel("Katzen", SwingConstants.CENTER);
		headings.add(katzenText);

		JLabel hundeText=new JLabel("Hunde", SwingConstants.CENTER);
		headings.add(hundeText);		

		this.add(headings, BorderLayout.PAGE_START);



		kuehe=new JPanel();
		kuehe.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		kuehe.setBackground(Color.LIGHT_GRAY);

		schafe=new JPanel();
		schafe.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		schafe.setBackground(Color.LIGHT_GRAY);

		katzen=new JPanel();
		katzen.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		katzen.setBackground(Color.LIGHT_GRAY);

		hunde=new JPanel();
		hunde.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		hunde.setBackground(Color.LIGHT_GRAY);


		JPanel spalten=new JPanel();

		spalten.setPreferredSize(new Dimension(800, 450));
		spalten.setLayout(new GridLayout(0,4));
		spalten.setBackground(Color.LIGHT_GRAY);

		spalten.add(kuehe);
		spalten.add(schafe);
		spalten.add(katzen);
		spalten.add(hunde);

		this.add(spalten, BorderLayout.CENTER);

		JPanel unten=new JPanel();
		unten.setLayout(new BorderLayout());
		unten.setPreferredSize(new Dimension(20,35));
		unten.setBorder(new MatteBorder(0, 1, 1, 1, Color.BLACK));

		ausgabe=new JTextArea();
		ausgabe.setEditable(false);
		ausgabe.setPreferredSize(new Dimension(800,35));
		ausgabe.getMaximumSize();
		ausgabe.setFont(new Font("Verdana",Font.PLAIN, 35));
		ausgabe.setBackground(Color.DARK_GRAY);
		ausgabe.setForeground(Color.WHITE);
		unten.add(ausgabe,BorderLayout.CENTER);

		JPanel knoepfe=new JPanel(new BorderLayout());

		JButton addTier=new JButton("Neues Tier");
		addTier.addActionListener(new addListener());
		addTier.setPreferredSize(new Dimension(150,35));
		knoepfe.add(addTier,BorderLayout.EAST);

		JButton delTier=new JButton("Tier freilassen");
		delTier.addActionListener(new delListener());
		delTier.setPreferredSize(new Dimension(150,35));
		knoepfe.add(delTier,BorderLayout.WEST);

		unten.add(knoepfe,BorderLayout.EAST);

		this.add(unten, BorderLayout.PAGE_END);

		tiereAnzeigen();
	}
	public static void tiereAnzeigen() {
		kuehe.removeAll();
		schafe.removeAll();
		katzen.removeAll();
		hunde.removeAll();

		JButton neuesTierGUI;
		for(int i=0;i<MyLittleZoo.alleTiere.size();i++) {
			switch(MyLittleZoo.alleTiere.get(i).getArt()){
			case "Kuh": {
				neuesTierGUI = new TierGUI(MyLittleZoo.alleTiere.get(i));
				kuehe.add(neuesTierGUI);
			} break;
			case "Schaf": {
				neuesTierGUI = new TierGUI(MyLittleZoo.alleTiere.get(i));
				schafe.add(neuesTierGUI);
			} break;
			case "Katze": {
				neuesTierGUI = new TierGUI(MyLittleZoo.alleTiere.get(i));
				katzen.add(neuesTierGUI);
			} break;
			case "Hund": {
				neuesTierGUI = new TierGUI(MyLittleZoo.alleTiere.get(i));
				hunde.add(neuesTierGUI);
			} break;
			}
		}
		drawNew();
	}
	public class addListener implements ActionListener {
		public void actionPerformed(ActionEvent ev) {
			JFrame neuesTier=new JFrame();
			neuesTier.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
			neuesTier.setTitle("Neues Tier");
			neuesTier.setSize(500,120);
			neuesTier.setResizable(false);
			neuesTier.setLayout(new BorderLayout());;
			neuesTier.setVisible(true);

			JPanel tier=new JPanel(new GridLayout(1,0));

			JLabel tierText=new JLabel("Name des Tieres:", SwingConstants.CENTER);
			JTextField tierField=new JTextField();

			tier.add(tierText);
			tier.add(tierField);
			tier.setPreferredSize(new Dimension(300,30));

			neuesTier.add(tier, BorderLayout.PAGE_START);

			JPanel tierButtons=new JPanel();

			JRadioButton kuhButton = new JRadioButton("Kuh");
			kuhButton.setMnemonic(KeyEvent.VK_C);

			JRadioButton schafButton = new JRadioButton("Schaf");
			schafButton.setMnemonic(KeyEvent.VK_C);

			JRadioButton katzeButton = new JRadioButton("Katze");
			katzeButton.setMnemonic(KeyEvent.VK_C);

			JRadioButton hundButton = new JRadioButton("Hund");
			hundButton.setMnemonic(KeyEvent.VK_C);

			ButtonGroup group = new ButtonGroup();
			group.add(kuhButton);
			group.add(schafButton);
			group.add(katzeButton);
			group.add(hundButton);

			tierButtons.add(kuhButton);
			tierButtons.add(schafButton);
			tierButtons.add(katzeButton);
			tierButtons.add(hundButton);

			ArrayList<JRadioButton>radioButton=new ArrayList<JRadioButton>();
			radioButton.add(kuhButton);
			radioButton.add(schafButton);
			radioButton.add(katzeButton);
			radioButton.add(hundButton);

			neuesTier.add(tierButtons, BorderLayout.CENTER);

			JPanel button=new JPanel(new GridLayout(1,0));

			JButton cancel=new JButton("Cancel");
			JButton adden=new JButton("Hinzufügen");

			cancel.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent e) {
					neuesTier.dispose();
				}
			});
			adden.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent arg0) {
					MyLittleZoo.neuesTier(radioButton, tierField);
					tiereAnzeigen();
					neuesTier.dispose();
				}
			});

			button.add(cancel);
			button.add(adden);

			neuesTier.add(button, BorderLayout.PAGE_END);
		}
	}
	public class delListener implements ActionListener {
		public void actionPerformed(ActionEvent ev) {
			JFrame tierFreilassen=new JFrame();
			tierFreilassen.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
			tierFreilassen.setTitle("Tier freilassen");
			tierFreilassen.setSize(500,300);
			tierFreilassen.setResizable(false);
			tierFreilassen.setLayout(new BorderLayout());;
			tierFreilassen.setVisible(true);

			JPanel tierBoxes=new JPanel();
			ArrayList<JCheckBox> checkBoxes=new ArrayList<JCheckBox>();

			for(int i=0;i<MyLittleZoo.alleTiere.size();i++) {
				JCheckBox tier=new JCheckBox(MyLittleZoo.alleTiere.get(i).getName());
				tier.setMnemonic(KeyEvent.VK_C);
				tierBoxes.add(tier);
				checkBoxes.add(tier);
			}

			tierFreilassen.add(tierBoxes, BorderLayout.CENTER);

			JPanel button=new JPanel(new GridLayout(1,0));

			JButton cancel=new JButton("Cancel");
			JButton freilassen=new JButton("Freilassen");

			cancel.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent e) {
					tierFreilassen.dispose();
				}
			});
			freilassen.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent arg0) {
					MyLittleZoo.tiereFreilassen(checkBoxes);
					tierFreilassen.dispose();
				}
			});

			button.add(cancel);
			button.add(freilassen);

			tierFreilassen.add(button, BorderLayout.PAGE_END);
		}		
	}
	public static void drawNew() {
		kuehe.repaint();
		kuehe.revalidate();
		schafe.repaint();
		schafe.revalidate();
		katzen.repaint();
		katzen.revalidate();
		hunde.repaint();
		hunde.revalidate();
	}
}