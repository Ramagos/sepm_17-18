import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

public class TierGUI extends JButton{
	Tier tier;
	public TierGUI(Tier tier) {
		this.tier=tier;
		this.setBackground(new Color(0,0,0,0));
		this.setBorder(null);
		switch(tier.getArt()) {
		case "Kuh": {
			this.setIcon(new ImageIcon("ressources/kuh.png"));
		} break;
		case "Schaf": {
			this.setIcon(new ImageIcon("ressources/schaf.png"));
		} break;
		case "Katze": {
			this.setIcon(new ImageIcon("ressources/katze.png"));
		} break;
		case "Hund": {
			this.setIcon(new ImageIcon("ressources/hund.png"));
		} break;
		}
		this.addActionListener(new Listener());
	}
	public class Listener implements ActionListener {
		public void actionPerformed(ActionEvent ev) {
			View.ausgabe.setText(tier.greet());
		}
	}
}
