import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public abstract class Tier implements Comparable<Tier>, Serializable{
	public String name;
	
	public Tier(String name){
		this.name=name;
	}
	
	public abstract String getGerausch();
	
	public String getArt(){
		return this.getClass().toString().substring(6);
	}
	public String greet(){
		return "Mein Name ist "+this.getName()+", i bims 1 "+this.getClass().toString().substring(6)+" und ich mache "+this.getGerausch();
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public int compareTo(Tier a2){
		return this.name.compareTo(a2.name);
	}
	
}