public class rekursiv {

	public static void main(String[] args) {
		System.out.println(rekFac(8));
		System.out.println(pascal(5,3));
		System.out.println(isPalindrom("anna"));
		System.out.println(isPalindrom("peter"));
		System.out.println(isPalindrom("lagerregal"));
		System.out.println(entRekFac(8));
		//System.out.println(entPascal(5,3));
		int[] array = {0,1,2,3,4,5,6,7,8,9,10};
		System.out.println(binarySearch(array, 3));
	}

	public static int pascal(int i, int k){
		if((k==0)||(k==i))
			return 1;

		return pascal(i-1,k-1) + pascal(i-1,k);
	}

	/*public static int entPascal(int i, int k){
		return entPascal(i, k, 1, 0);
	}

	private static int entPascal(int i, int k, int res1, int res2){
		if((k==0)||(k==i))
			return res1+res2;

		int help;
		if(res1<res2)
			help=res2;
		else help=res1;

		return entPascal(i-1, k-1, res1+res2, help);
	}*/

	public static int entRekFac(int n){
		return entRekFac(n, 1);
	}

	private static int entRekFac(int n, int res){
		if(n==2)
			return res*2;
		return entRekFac(n-1, res*n);
	}

	public static int rekFac(int n){
		if(n==2)
			return 2;
		return n*rekFac(n-1);
	}

	public static boolean isPalindrom(String pal){
		if((pal.length()==0)||(pal.length()==1))
			return true;

		if(pal.charAt(0)==pal.charAt(pal.length()-1))
			return isPalindrom(pal.substring(1, pal.length()-1));
		else 
			return false;
	}

	public static int binarySearch(int[] array, int n){
		return binarySearch(array, n, 0, array.length-1);
	}

	private static int binarySearch(int[] array, int n, int anfang, int ende){
		if (ende >= anfang)
		{
			int mitte = anfang + (ende - anfang)/2;

			if (array[mitte] == n)  
				return mitte;

			if (array[mitte] > n) 
				return binarySearch(array, n, anfang, mitte-1);

			return binarySearch(array, n, mitte+1, ende);
		}

		return -1;
	}
}