import java.awt.Color;
import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.*;
import java.net.*;
import java.util.ArrayList;
import javax.swing.*;
import java.util.regex.*;

public class Main {

	static ArrayList<String> md = new ArrayList<String>();
	static String htmlF="<html>"+"\n"+"<head>"+"\n"+"\n"+"</head>"+"\n"+"<body>"+"\n";
	static String html=" ";
	static String line = null;

	public static void main(String[] args) {
		try{
			loadData();

			setMdSegment(0, getMd().get(0).replace("﻿", ""));
			ConvertToHtml(getMd());
			setHtml(getHtml().replace("ü", "&uuml;"));

			setHtmlF(getHtmlF()+getHtml());
			setHtmlF(getHtmlF() + "\n</body>\n</html>");
			saveData();

			//openBrowser();

		}catch (Exception e){
			System.out.println(e.getMessage());
		}
	}

	public static void ConvertToHtml(ArrayList<String> md){
		boolean shell = false;
		boolean change = false;

		for(int i=0;i<md.size();i++){
			setHtml(getHtml() + "\n");
			change = false;
			System.out.println(getMdSegment(i));
			if(!getMdSegment(i).equals(null)){
				while(getMdSegment(i).contains("#")){
					if(getMdSegment(i).contains("######")){
						setMdSegment(i, getMdSegment(i).replaceFirst("######", "<h6>"));
						setMdSegment(i, getMdSegment(i)+"</h6>");
					}
					if(getMdSegment(i).contains("#####")){
						setMdSegment(i, getMdSegment(i).replaceFirst("#####", "<h5>"));
						setMdSegment(i, getMdSegment(i)+"</h5>");
					}
					if(getMdSegment(i).contains("####")){
						setMdSegment(i, getMdSegment(i).replaceFirst("####", "<h4>"));
						setMdSegment(i, getMdSegment(i)+"</h4>");
					}
					if(getMdSegment(i).contains("###")){
						setMdSegment(i, getMdSegment(i).replaceFirst("###", "<h3>"));
						setMdSegment(i, getMdSegment(i)+"</h3>");
					}
					if(getMdSegment(i).contains("##")){
						setMdSegment(i, getMdSegment(i).replaceFirst("##", "<h2>"));
						setMdSegment(i, getMdSegment(i)+"</h2>");
					}
					if(getMdSegment(i).contains("#")){
						setMdSegment(i, getMdSegment(i).replaceFirst("#", "<h1>"));
						setMdSegment(i, getMdSegment(i)+"</h1>");
					}

					if(!getMdSegment(i).contains("#")){
						setHtml(getHtml() + getMdSegment(i));
					}
					change = true;
				}

				if((getMdSegment(i).contains("---") 
						&& (!getMdSegment(i).contains("------")))){
					setMdSegment(i, getMdSegment(i).replaceFirst("---", "<hr>"));
					setHtml(getHtml() + getMdSegment(i));
					change = true;
				}

				if((getMdSegment(i).contains("* ") 
						&& (!getMdSegment(i).contains("***") 
								&& (!getMdSegment(i).contains("**")) 
								&& (!getMdSegment(i).contains("*"+"\u0020"))))){
					setHtml(getHtml() + "<ul>");
					setMdSegment(i, getMdSegment(i).replaceFirst("[*]", "<li>"));
					setHtml(getHtml() + getMdSegment(i));
					setHtml(getHtml() + "</li>"+"</ul>");
					change = true;
				}

				if((getMdSegment(i).contains("- ")) 
						&& (!getMdSegment(i).contains("--"))){
					setHtml(getHtml() + "<ul> \n");
					while(getMdSegment(i).contains("- ")){
						setMdSegment(i, getMdSegment(i).replaceFirst("-", "<li>"));
						setHtml(getHtml() + getMdSegment(i) + "</li>\n");
						i++;
					}
					setHtml(getHtml() + "</ul>");
					change = true;
				}
				
				if(getMdSegment(i).contains("|")){
					int counter;
					setHtml(getHtml() + "<table border=\"1px\"> \n");
					
					while(getMdSegment(i).contains("|")){
						setMdSegment(i, getMdSegment(i).replaceFirst("[|]", "<tr>\n<th>"));
						counter = -2;
						char a[] = getMdSegment(i).toCharArray();
						for(char z : a)
							counter += '|' == z ? 1 : 0;
						
						for(int j=0;j<=counter;j++)
							setMdSegment(i, getMdSegment(i).replaceFirst("[|]", "</th>\n<th>"));
						setMdSegment(i, getMdSegment(i).replaceFirst("[|]", "</th>\n"));
						
						setHtml(getHtml() + getMdSegment(i));
						i++;
					}
					setMdSegment(i, getMdSegment(i).replaceFirst("[|]", "</tr>"));
					
					setHtml(getHtml() + "</tr>\n</table>");
					change = true;
				}

				while(getMdSegment(i).contains("*")){
					setMdSegment(i, getMdSegment(i).replaceFirst("[*][*][*]", " <b><i>"));
					setMdSegment(i, getMdSegment(i).replaceFirst("[*][*][*]", "</b></i> "));
					if(!getMdSegment(i).contains("***")){
						setMdSegment(i, getMdSegment(i).replaceFirst("[*][*]", " <b>"));
						setMdSegment(i, getMdSegment(i).replaceFirst("[*][*]", "</b> "));
						if(!getMdSegment(i).contains("**")){
							setMdSegment(i, getMdSegment(i).replaceFirst("[*]", " <i>"));
							setMdSegment(i, getMdSegment(i).replaceFirst("[*]", "</i> "));
						}
					}
					if(!getMdSegment(i).contains("*")){
						setHtml(getHtml() + getMdSegment(i));
						setHtml(getHtml() + "</br>");
					}
					change = true;
				}

				if((getMdSegment(i).contains("```")) 
						&& (!shell)){
					setMdSegment(i, getMdSegment(i).replaceFirst("```sh", "<pre><code class=\"language-sh\">"));
					setHtml(getHtml() + getMdSegment(i));
					shell=true;
					change = true;
				}else if((getMdSegment(i).contains("```")) 
						&& (shell)){
					setMdSegment(i, getMdSegment(i).replaceFirst("```", "</code></pre>"));
					setHtml(getHtml() + getMdSegment(i));
					shell=false;
					change = true;
				}
				if((shell)
						&&(!getMdSegment(i).contains("<pre><code class=\"language-sh\">"))){
					setHtml(getHtml() + getMdSegment(i));
					change = true;
				}

				if(!change){
					if(getMdSegment(i).equals(""))
						setHtml(getHtml() + "</br>");
					else 
						setHtml(getHtml() + getMdSegment(i));
				}
			}	
		}
	}

	public static void saveData(){
		try{
			setLine(getLine().replaceFirst(".md", "") + ".html");
			BufferedWriter bw=new BufferedWriter(new FileWriter(getLine()));
			bw.write(getHtmlF());
			bw.close();
		}catch (Exception e){
			System.out.println(e.getMessage());
		}
	}

	public static void loadData(){
		String einzel=null;
		boolean help=true;
		BufferedReader console = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Bitte Dateipfad angeben:");

		try{
			setLine(console.readLine());
			BufferedReader br=new BufferedReader(new FileReader(getLine()));
			while (help) {
				einzel=br.readLine();
				if(einzel!=null)
					getMd().add(einzel+" ");
				else help=false;
			}
			br.close();
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
	}

	public static void openBrowser() throws IOException, URISyntaxException{
		try{
			setLine(new File(getLine()).toURI().toString());

			String os = System.getProperty("os.name").toLowerCase();
			Runtime rt = Runtime.getRuntime();

			if(os.indexOf("win") >= 0)
				rt.exec("rundll32 url.dll,FileProtocolHandler " + getLine());
			else if(os.indexOf("nix") >=0 || os.indexOf("nux") >=0){
				String[] browsers = { "epiphany", "firefox", "mozilla", "konqueror", "netscape", "opera", "links", "lynx" };

				StringBuffer cmd = new StringBuffer();
				for (int i = 0; i < browsers.length; i++)
					if(i == 0)
						cmd.append(String.format(    "%s \"%s\"", browsers[i], getLine()));
					else
						cmd.append(String.format(" || %s \"%s\"", browsers[i], getLine())); 
				rt.exec(new String[] { "sh", "-c", cmd.toString() });

			}else if(os.indexOf("mac") >= 0)
				rt.exec("open " + getLine());

		}catch (Exception e){
			System.out.println(e.getMessage());
		}
	}

	public static ArrayList<String> getMd() {
		return md; }
	public static String getMdSegment(int i) {
		return md.get(i); }
	public static void setMdSegment(int i, String mdSet) {
		md.set(i, mdSet); }

	public static String getHtml() {
		return html; }
	public static void setHtml(String htmlSet) {
		html = htmlSet; }

	public static String getHtmlF() {
		return htmlF; }
	public static void setHtmlF(String htmlFSet) {
		htmlF = htmlFSet; }

	public static String getLine() {
		return line; }
	public static void setLine(String lineSet) {
		line = lineSet; }
}