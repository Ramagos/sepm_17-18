public class insertionsort {
	public static void main(String[] args) {
		MyLinkedList mll = new MyLinkedList();
		mll.add(11);
		mll.add(4);
		mll.add(12);
		mll.addPos(0, 14);
		mll.add(2);
		mll.add(18);
		mll.add(10);
		mll.add(1);
		mll.add(19);
		mll.add(17);
		mll.add(200);

		Node node = mll.getHead();
		do {
			System.out.println(node.getValue());
			node = node.getNext();
		} while (node.hasNext());
		System.out.println(node.getValue());
		System.out.println();
		
		mll=sort(mll);
		
		node = mll.getHead();
		do {
			System.out.println(node.getValue());
			node = node.getNext();
		} while (node.hasNext());
		System.out.println(node.getValue());
	}
	
	public static MyLinkedList sort(MyLinkedList sortieren){
		MyLinkedList sortiert=new MyLinkedList();
		sortiert.add(sortieren.getValue(0));
		boolean schleife;
		for(int i=1;i<sortieren.getSize();i++){
			schleife=true;
			for (int j = 0; j < sortiert.getSize() && schleife; j++) {
				if ((sortiert.getValue(j) >= sortieren.getValue(i))){
					sortiert.addPos(j, sortieren.getValue(i));
					schleife=false;
				}
				if((j==sortiert.getSize()-1)&&schleife)
					sortiert.add(sortieren.getValue(i));
			}
		}
		
		return sortiert;
	}
}