import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Random;

public class Main {

	public static void main(String[] args) throws NumberFormatException, IOException {
		ArrayList<Integer> output=new ArrayList<Integer>();
		int length;
		
		BufferedReader console = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Bitte Arraylänge angeben:");
		
		length = Integer.parseInt(console.readLine());
		
		output=sort(length);
		System.out.println("Sortiert:");
		for(int i=0;i<output.size();i++){
			System.out.println(output.get(i));
		}
	}

	public static ArrayList<Integer> sort(int length) {
		ArrayList<Integer> input=new ArrayList<Integer>();
		
		Random randomGenerator = new Random();
		for(int i=0;i<length;i++){
			input.add(randomGenerator.nextInt(100));
			System.out.println(input.get(i));
		}
		
		int help;
		for(int i=1; i<input.size(); i++) {
			for(int j=0; j<input.size()-i; j++) {
				if(input.get(j)>input.get(j+1)) {
					help=input.get(j);
					input.set(j, input.get(j+1));
					input.set(j+1, help);
				}
			}
		}
		return input;
	}
}