import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Random;

public class shellsort {

	public static void main(String[] args) throws NumberFormatException, IOException {
		ArrayList<Integer> output=new ArrayList<Integer>();
		int length;
		int time;

		BufferedReader console = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Bitte Arraylänge angeben:");

		length = Integer.parseInt(console.readLine());

		time=(int)System.currentTimeMillis();

		output=sort(length);
		System.out.println("Sortiert:");
		for(int i=0;i<output.size();i++){
			System.out.println(output.get(i));
		}
		time=(int)System.currentTimeMillis()-time;
		System.out.println();
		System.out.println(time);
	}

	public static ArrayList<Integer> sort(int length) {
		ArrayList<Integer> input=new ArrayList<Integer>();

		Random randomGenerator = new Random();
		for(int i=0;i<length;i++){
			input.add(randomGenerator.nextInt(20000));
			System.out.println(input.get(i));
		}

		int inner, outer;
		long temp;
		//find initial value of h
		int h = 1;
		while (h <= length / 3)
			h = h * 3 + 1; // (1, 4, 13, 40, 121, ...)

		while (h > 0) // decreasing h, until h=1
		{
			// h-sort the file
			for (outer = h; outer < length; outer++) {
				temp = input.get(outer);
				inner = outer;
				// one subpass (eg 0, 4, 8)
				while (inner > h - 1 && input.get(inner - h) >= temp) {
					input.set(inner,input.get(inner - h));
					inner -= h;
				}
				input.set(inner, (int)temp);
			}
			h = (h - 1) / 3; // decrease h
		}
		return input;
	}
}