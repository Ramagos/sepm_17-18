import java.util.Arrays;

public class quicksort {
	public static void main(String args[]) {
		
        int[] input = {21,23,52,19,4,12,48};
        System.out.print(Arrays.toString(input) + " -> ");
        
        rekQuicksort(input); 
        
        System.out.println(Arrays.toString(input));
    }

    public static void rekQuicksort(int[] array) {
    	rekQuicksort(array, 0, array.length-1);
    }

    public static void rekQuicksort(int[] array, int startIndex, int endeIndex) {
        int index = pivot(array, startIndex, endeIndex);

        if (startIndex < index - 1) {
        	rekQuicksort(array, startIndex, index - 1);
        }

        if (endeIndex > index) {
        	rekQuicksort(array, index, endeIndex);
        }
    }

    public static int pivot(int[] array, int links, int rechts) {
        int pivot = array[links];

        while (links <= rechts) {
            while (array[links] < pivot) {
                links++;
            }
            while (array[rechts] > pivot) {
                rechts--;
            }

            if (links <= rechts) {
                swap(array, links, rechts);

                links++;
                rechts--;
            }
        }
        return links;
    }
    
    private static void swap(int[] array, int index1, int index2){
    	int help=array[index1];
    	array[index1]=array[index2];
    	array[index2]=help;
    }
}