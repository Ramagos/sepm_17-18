import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.Serializable;
import java.util.Locale;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

public class Storycard extends JButton implements Serializable{
	public String arbeiter;
	public String funktion;
	public boolean subcard;
	Storycard[] subcards = new Storycard[4];
	String panel;
	GUI parent;

	public Storycard(String arbeiter, String funktion, GUI parent) {
		super("", new ImageIcon("ressources/storycard.jpg"));
		this.arbeiter = arbeiter;
		this.funktion = funktion;
		this.subcard=false;
		this.panel="ideen";
		this.parent=parent;

		this.setHorizontalTextPosition(SwingConstants.CENTER);
		this.setVerticalTextPosition(SwingConstants.TOP);

		this.setText("<html>Person: " + arbeiter + "<br>Funktion: " + funktion + "</html>");
		this.setIconTextGap(-31);
		this.setBackground(new Color(0,0,0,0));
		this.setBorder(null);

		this.addActionListener(new Listener());
	}

	public Storycard(String arbeiter, String funktion, boolean subcard) {
		super();
		this.arbeiter = arbeiter;
		this.funktion = funktion;
		this.subcard = subcard;
	}

	public class Listener implements ActionListener {
		public void actionPerformed(ActionEvent ev) {
			JFrame subFrame=new JFrame();

			int x=325;
			int y=55;

			Component[] parentPanel=new Component[1];

			switch(panel) {
			case "ideen": parentPanel=parent.ideen.getComponents();
			break;
			case "toDo": parentPanel=parent.toDo.getComponents();
			break;
			case "doing": parentPanel=parent.doing.getComponents();
			break;
			case "abgeschlossen": parentPanel=parent.abgeschlossen.getComponents();
			break;
			}

			for(int i=0;i<5;i++) {
				if((Storycard)parentPanel[i]==Storycard.this) {
					y=i;
					break;
				}
			}

			y=y*170;

			subFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
			subFrame.setLayout(new BorderLayout());
			subFrame.setUndecorated(true);
			subFrame.setBackground(new Color(0,0,0,0));
			subFrame.setLocation(new Point(x,y));

			JButton subcardIMG=new JButton();

			int subs=0;
			for(int i=0;i<subcards.length;i++) {
				if(subcards[i]!=null)
					subs++;
			}

			switch(subs) {
			case 1:{
				subcardIMG=new JButton("", new ImageIcon("ressources/sub1.png"));
				subcardIMG.setHorizontalTextPosition(SwingConstants.CENTER);
				subcardIMG.setVerticalTextPosition(SwingConstants.TOP);

				subcardIMG.setText("<html>Person: " + subcards[0].arbeiter+"<br>Funktion: " + subcards[0].funktion + "</html>");
				subcardIMG.setIconTextGap(-31);
				subcardIMG.setBackground(new Color(0,0,0,0));
				subcardIMG.setBorder(null);
				subFrame.setSize(300,150);
			}
			break;
			case 2:{
				JLabel pfeilanfang=new JLabel(new ImageIcon("ressources/pfeilanfang.jpg"));
				pfeilanfang.setSize(new Dimension(9,2));
				subFrame.add(pfeilanfang,BorderLayout.WEST);
				subcardIMG=new JButton("", new ImageIcon("ressources/sub2.png"));
				subcardIMG.setHorizontalTextPosition(SwingConstants.CENTER);
				subcardIMG.setVerticalTextPosition(SwingConstants.TOP);

				subcardIMG.setText("<html>"
						+"Person: " + subcards[0].arbeiter+"<br>Funktion: " + subcards[0].funktion +"<br><br><br><br><br><br><br><br><br><br>"
						+"Person: " + subcards[1].arbeiter+"<br>Funktion: " + subcards[1].funktion 
						+ "</html>");
				subcardIMG.setIconTextGap(-197);
				subcardIMG.setBackground(new Color(0,0,0,0));
				subcardIMG.setBorder(null);
				subFrame.setSize(300,320);
			}
			break;
			case 3:{
				JLabel pfeilanfang=new JLabel(new ImageIcon("ressources/pfeilanfang.jpg"));
				pfeilanfang.setSize(new Dimension(9,2));
				subFrame.add(pfeilanfang,BorderLayout.WEST);
				subcardIMG=new JButton("",new ImageIcon("ressources/sub3.png"));
				subcardIMG.setHorizontalTextPosition(SwingConstants.CENTER);
				subcardIMG.setVerticalTextPosition(SwingConstants.TOP);

				subcardIMG.setText("<html>"
						+"Person: " + subcards[0].arbeiter+"<br>Funktion: " + subcards[0].funktion +"<br><br><br><br><br><br><br><br><br><br>"
						+"Person: " + subcards[1].arbeiter+"<br>Funktion: " + subcards[1].funktion +"<br><br><br><br><br><br><br><br><br><br>"
						+"Person: " + subcards[2].arbeiter+"<br>Funktion: " + subcards[2].funktion
						+ "</html>");
				subcardIMG.setIconTextGap(-360);
				subcardIMG.setBackground(new Color(0,0,0,0));
				subcardIMG.setBorder(null);
				subFrame.setSize(300,490);
			}
			break;
			case 4:{
				JLabel pfeilanfang=new JLabel(new ImageIcon("ressources/pfeilanfang.jpg"));
				pfeilanfang.setSize(new Dimension(9,2));
				subFrame.add(pfeilanfang,BorderLayout.WEST);
				subcardIMG=new JButton(new ImageIcon("ressources/sub4.png"));
				subcardIMG.setHorizontalTextPosition(SwingConstants.CENTER);
				subcardIMG.setVerticalTextPosition(SwingConstants.TOP);

				subcardIMG.setText("<html>"
						+"Person: " + subcards[0].arbeiter+"<br>Funktion: " + subcards[0].funktion +"<br><br><br><br><br><br><br><br><br><br>"
						+"Person: " + subcards[1].arbeiter+"<br>Funktion: " + subcards[1].funktion +"<br><br><br><br><br><br><br><br><br><br>"
						+"Person: " + subcards[2].arbeiter+"<br>Funktion: " + subcards[2].funktion +"<br><br><br><br><br><br><br><br><br><br>"
						+"Person: " + subcards[3].arbeiter+"<br>Funktion: " + subcards[3].funktion
						+ "</html>");
				subcardIMG.setIconTextGap(-524);
				subcardIMG.setBackground(new Color(0,0,0,0));
				subcardIMG.setBorder(null);
				subFrame.setSize(300,660);
			}
			break;
			}

			subFrame.setVisible(true);

			subFrame.add(subcardIMG);

			JFrame close=new JFrame();
			close.setUndecorated(true);
			close.setBackground(new Color(0,0,0,0));
			close.setSize(new Dimension(1600,900));
			close.addMouseListener(new MouseAdapter() {
				public void mouseClicked(MouseEvent e) {
					close.dispose();
					subFrame.dispose();
					parent.drawNew();
				}
			});
			close.setVisible(true);
		}
	}

	public boolean isSubcard() {
		return subcard;
	}

	public GUI getParent() {
		return parent;
	}

	public void setParent(GUI parent) {
		this.parent = parent;
	}

	public void setSubcard(boolean subcard) {
		this.subcard = subcard;
	}

	public String getArbeiter() {
		return arbeiter;
	}

	public String getFunktion() {
		return funktion;
	}

	public void setArbeiter(String arbeiter) {
		this.arbeiter = arbeiter;
	}

	public void setFunktion(String funktion) {
		this.funktion = funktion;
	}

	public Storycard[] getSubcards() {
		return subcards;
	}

	public String getPanel() {
		return panel;
	}

	public void setSubcards(Storycard[] subcards) {
		this.subcards = subcards;
	}

	public void setPanel(String panel) {
		this.panel = panel;
	}
}