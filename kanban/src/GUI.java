import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.swing.*;
import javax.swing.border.*;

public class GUI extends JFrame{
	public JPanel ideen=new JPanel();
	public JPanel toDo=new JPanel();
	public JPanel doing=new JPanel();
	public JPanel abgeschlossen=new JPanel();
	public JPanel muell=new JPanel();

	JPanel muellSpalte=new JPanel();
	
	ScriptEngineManager manager = new ScriptEngineManager();
    ScriptEngine engine = manager.getEngineByName("test");

	public GUI() {
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setTitle("Kanban");
		this.setSize(800,500);
		this.setLayout(new BorderLayout());
		this.setVisible(true);


		//Headings
		JPanel headings=new JPanel();

		headings.setPreferredSize(new Dimension(800,25));
		headings.setLayout(new GridLayout(0,4));
		headings.setBackground(Color.LIGHT_GRAY);

		JLabel ideenText=new JLabel("Ideen", SwingConstants.CENTER);
		headings.add(ideenText);

		JLabel toDoText=new JLabel("toDo", SwingConstants.CENTER);
		headings.add(toDoText);

		JLabel doingText=new JLabel("doing", SwingConstants.CENTER);
		headings.add(doingText);

		JLabel abgeschlossenText=new JLabel("done", SwingConstants.CENTER);
		headings.add(abgeschlossenText);		

		this.add(headings, BorderLayout.PAGE_START);



		//Spalten

		ideen.setBorder(BorderFactory.createLineBorder(Color.BLACK));

		Storycard test=new Storycard("Florian Bartenbach","test",this);
		ideen.add(test);


		JPanel umsetzung=new JPanel();

		umsetzung.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		umsetzung.setLayout(new GridLayout(2,0));

		umsetzung.add(toDo);

		muellSpalte.setLayout(new BorderLayout());
		muellSpalte.setBackground(Color.LIGHT_GRAY);

		JLabel muellText=new JLabel("M�ll", SwingConstants.CENTER);
		muellText.setHorizontalAlignment(JLabel.CENTER);
		muellText.setPreferredSize(new Dimension(200,25));
		muellSpalte.add(muellText, BorderLayout.PAGE_START);


		muell.setBorder(new MatteBorder(1, 0, 0, 0, Color.BLACK));
		muellSpalte.add(muell, BorderLayout.CENTER);

		umsetzung.add(muellSpalte);

		doing.setBorder(BorderFactory.createLineBorder(Color.BLACK));

		abgeschlossen.setBorder(BorderFactory.createLineBorder(Color.BLACK));


		JPanel spalten=new JPanel();

		spalten.setPreferredSize(new Dimension(800, 450));
		spalten.setLayout(new GridLayout(0,4));
		spalten.setBackground(Color.GREEN);

		spalten.add(ideen);
		spalten.add(umsetzung);
		spalten.add(doing);
		spalten.add(abgeschlossen);

		this.add(spalten, BorderLayout.CENTER);


		//Control

		JPanel knoepfe=new JPanel();
		knoepfe.setPreferredSize(new Dimension(800,35));
		knoepfe.setBorder(new MatteBorder(0, 1, 1, 1, Color.BLACK));

		JButton addStoryCard=new JButton("Neue Storycard");
		addStoryCard.addActionListener(new addListener());
		knoepfe.add(addStoryCard);

		JButton addSubCard=new JButton("Neue Subcard");
		addSubCard.addActionListener(new addSubListener());
		knoepfe.add(addSubCard);

		JButton moveStoryCard=new JButton("Storycard verschieben");
		moveStoryCard.addActionListener(new moveListener());
		knoepfe.add(moveStoryCard);

		JButton deleteStoryCard=new JButton("Storycard l�schen");
		deleteStoryCard.addActionListener(new deleteListener());
		knoepfe.add(deleteStoryCard);

		this.add(knoepfe, BorderLayout.PAGE_END);
	}

	public class addListener implements ActionListener {
		public void actionPerformed(ActionEvent ev) {
			JFrame neueStoryCard=new JFrame();
			neueStoryCard.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
			neueStoryCard.setTitle("Neue Storycard");
			neueStoryCard.setSize(500,300);
			neueStoryCard.setLayout(new BorderLayout());;
			neueStoryCard.setVisible(true);

			JPanel person=new JPanel(new GridLayout(1,0));

			JLabel personText=new JLabel("Zuständige Person: ", SwingConstants.CENTER);
			JTextField personField=new JTextField();

			person.add(personText);
			person.add(personField);
			person.setPreferredSize(new Dimension(300,30));

			neueStoryCard.add(person, BorderLayout.PAGE_START);

			JPanel funktion=new JPanel(new GridLayout(1,0));

			JLabel funktionText=new JLabel("Funktion der Storycard: ", SwingConstants.CENTER);
			JTextField funktionField=new JTextField();

			funktion.add(funktionText);
			funktion.add(funktionField);

			neueStoryCard.add(funktion, BorderLayout.CENTER);

			JPanel button=new JPanel(new GridLayout(1,0));

			JButton cancel=new JButton("Cancel");
			JButton adden=new JButton("Hinzufügen");

			cancel.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent e) {
					neueStoryCard.dispose();
				}
			});
			adden.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent e) {
					Storycard neu=new Storycard(personField.getText(), funktionField.getText(),GUI.this);
					ideen.add(neu);
					drawNew();
					neueStoryCard.dispose();
				}
			});

			button.add(cancel);
			button.add(adden);

			neueStoryCard.add(button, BorderLayout.PAGE_END);
		}
	}

	public class addSubListener implements ActionListener {		
		public void actionPerformed(ActionEvent ev) {
			JFrame addSub=new JFrame();
			addSub.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
			addSub.setTitle("Neue Subcard");
			addSub.setSize(500,300);
			addSub.setLayout(new BorderLayout());;
			addSub.setVisible(true);

			JPanel erstellen=new JPanel(new BorderLayout());
			
			JPanel person=new JPanel(new GridLayout(1,0));

			JLabel personText=new JLabel("Zuständige Person: ", SwingConstants.CENTER);
			JTextField personField=new JTextField();

			person.add(personText);
			person.add(personField);
			person.setPreferredSize(new Dimension(300,30));

			erstellen.add(person, BorderLayout.PAGE_START);

			JPanel funktion=new JPanel(new GridLayout(1,0));

			JLabel funktionText=new JLabel("Funktion der Subcard: ", SwingConstants.CENTER);
			JTextField funktionField=new JTextField();

			funktion.add(funktionText);
			funktion.add(funktionField);

			erstellen.add(funktion, BorderLayout.CENTER);
			
			addSub.add(erstellen, BorderLayout.CENTER);
			
			JPanel mothercards=new JPanel(new GridLayout(9,0));

			ButtonGroup group=new ButtonGroup();

			ArrayList<Storycard> all=getAll();
			ArrayList<JCheckBox> checkBoxes=new ArrayList<JCheckBox>();

			for(int i=0;i<all.size();i++) {
				JCheckBox newCheckBox=new JCheckBox(all.get(i).getFunktion());
				newCheckBox.setMnemonic(KeyEvent.VK_C);
				group.add(newCheckBox);
				checkBoxes.add(newCheckBox);
				mothercards.add(newCheckBox);
			}
			
			addSub.add(mothercards, BorderLayout.EAST);

			
			JPanel button=new JPanel(new GridLayout(1,0));

			JButton cancel=new JButton("Cancel");
			JButton adden=new JButton("Hinzufügen");

			cancel.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent e) {
					addSub.dispose();
				}
			});
			adden.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent e) {
					Storycard neu=new Storycard(personField.getText(), funktionField.getText(), true);
					
					for(int i=0;i<checkBoxes.size();i++) {
						if(checkBoxes.get(i).isSelected()) {
							neu.setPanel(checkBoxes.get(i).getText()); //subcard
							for(int j=0;j<5;j++)
								if(all.get(i).subcards[j]==null) {
									all.get(i).subcards[j]=neu;
									break;
								}
							break;
						}
					}
					
					drawNew();
					addSub.dispose();
				}
			});

			button.add(cancel);
			button.add(adden);

			addSub.add(button, BorderLayout.PAGE_END);
		}
	}

	public class moveListener implements ActionListener {
		public void actionPerformed(ActionEvent ev) {
			JFrame move=new JFrame();
			move.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
			move.setTitle("Storycard verschieben");
			move.setSize(500,300);
			move.setLayout(new BorderLayout());;
			move.setVisible(true);

			JPanel checkPanel=new JPanel(new GridLayout(9,0));

			ArrayList<Storycard> all=getAll();
			ArrayList<JCheckBox> checkBoxes=new ArrayList<JCheckBox>();

			for(int i=0;i<all.size();i++) {
				JCheckBox newCheckBox=new JCheckBox(all.get(i).getFunktion());
				checkBoxes.add(newCheckBox);
				checkPanel.add(newCheckBox);
			}
			move.add(checkPanel, BorderLayout.CENTER);

			JPanel movePanel=new JPanel(new GridLayout(9,0));

			ButtonGroup group=new ButtonGroup();

			JCheckBox ideen = new JCheckBox("Ideen");
			ideen.setMnemonic(KeyEvent.VK_C);
			group.add(ideen);
			movePanel.add(ideen);

			JCheckBox toDo = new JCheckBox("toDo");
			toDo.setMnemonic(KeyEvent.VK_C);
			group.add(toDo);
			movePanel.add(toDo);

			JCheckBox doing = new JCheckBox("Doing");
			doing.setMnemonic(KeyEvent.VK_C);
			group.add(doing);
			movePanel.add(doing);

			JCheckBox abgeschlossen = new JCheckBox("Done");
			abgeschlossen.setMnemonic(KeyEvent.VK_C);
			group.add(abgeschlossen);
			movePanel.add(abgeschlossen);

			move.add(movePanel, BorderLayout.EAST);

			JPanel button=new JPanel(new GridLayout(0,2));

			JButton cancel=new JButton("Cancel");
			JButton moveButton=new JButton("Verschieben");

			cancel.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent e) {
					move.dispose();
				}
			});

			moveButton.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent e) {
					if(ideen.isSelected())
						move("ideen", checkBoxes,all);
					else 
						if(toDo.isSelected())
							move("toDo", checkBoxes,all);
						else 
							if(doing.isSelected())
								move("doing", checkBoxes,all);
							else 
								if(abgeschlossen.isSelected())
									move("abgeschlossen", checkBoxes,all);
					drawNew();
					move.dispose();
				}
			});

			button.add(cancel);
			button.add(moveButton);

			move.add(button, BorderLayout.PAGE_END);
		}
	}

	public class deleteListener implements ActionListener {
		public void actionPerformed(ActionEvent ev) {
			JFrame delete=new JFrame();
			delete.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
			delete.setTitle("Storycard löschen");
			delete.setSize(500,300);
			delete.setLayout(new BorderLayout());;
			delete.setVisible(true);

			JPanel checkPanel=new JPanel(new GridLayout(9,0));

			ArrayList<Storycard> all=getAll();
			ArrayList<JCheckBox> checkBoxes=new ArrayList<JCheckBox>();

			for(int i=0;i<all.size();i++) {
				JCheckBox newCheckBox=new JCheckBox(all.get(i).getFunktion());
				checkBoxes.add(newCheckBox);
				checkPanel.add(newCheckBox);
			}

			delete.add(checkPanel);

			JPanel button=new JPanel(new GridLayout(1,0));

			JButton cancel=new JButton("Cancel");
			JButton del=new JButton("Löschen");

			cancel.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent e) {
					delete.dispose();
				}
			});

			del.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent e) {
					move("muell", checkBoxes,all);
					drawNew();
					delete.dispose();
				}
			});

			button.add(cancel);
			button.add(del);

			delete.add(button, BorderLayout.PAGE_END);
		}
	}

	public  ArrayList<Storycard> getAll() {
		ArrayList<Storycard> all = new ArrayList<Storycard>();

		Component[] ideen1=ideen.getComponents();
		for(int i=0;i<ideen1.length;i++) {
			if(ideen1[i].getClass().equals(Storycard.class))
				all.add((Storycard) ideen1[i]);
		}

		Component[] doing1=doing.getComponents();
		for(int i=0;i<doing1.length;i++) {
			if(doing1[i].getClass().equals(Storycard.class))
				all.add((Storycard) doing1[i]);
		}

		Component[] toDo1=toDo.getComponents();
		for(int i=0;i<toDo1.length;i++) {
			if(toDo1[i].getClass().equals(Storycard.class))
				all.add((Storycard) toDo1[i]);
		}

		Component[] abgeschlossen1=abgeschlossen.getComponents();
		for(int i=0;i<abgeschlossen1.length;i++) {
			if(abgeschlossen1[i].getClass().equals(Storycard.class))
				all.add((Storycard) abgeschlossen1[i]);
		}

		return all;			
	}

	public void drawNew() {
		ideen.repaint();
		ideen.revalidate();
		toDo.repaint();
		toDo.revalidate();
		muell.repaint();
		muell.revalidate();
		doing.repaint();
		doing.revalidate();
		abgeschlossen.repaint();
		abgeschlossen.revalidate();
	}

	public void move(String ort, ArrayList<JCheckBox> checkBoxes, ArrayList<Storycard> all) {
		for(int i=0;i<checkBoxes.size();i++) {
			if(ort.equals(all.get(i).getPanel()))
				continue;
			if(checkBoxes.get(i).isSelected()) {
				if(ort.equals("muell"))
					muell.add(all.get(i));
				else 
					if(ort.equals("ideen"))
						ideen.add(all.get(i));
					else 
						if(ort.equals("toDo"))
							toDo.add(all.get(i));
						else 
							if(ort.equals("doing"))
								doing.add(all.get(i));
							else 
								if(ort.equals("abgeschlossen"))
									abgeschlossen.add(all.get(i));

				if(all.get(i).getPanel().equals("ideen")) {
					all.get(i).setPanel(ort);
					ideen.remove(all.get(i));
				}
				else 
					if(all.get(i).getPanel().equals("toDo")){
						all.get(i).setPanel(ort);
						toDo.remove(all.get(i));
					}
					else 
						if(all.get(i).getPanel().equals("doing")){
							all.get(i).setPanel(ort);
							doing.remove(all.get(i));
						}
						else
							if(all.get(i).getPanel().equals("abgeschlossen")){
								all.get(i).setPanel(ort);
								abgeschlossen.remove(all.get(i));
							}
			}
		}
	}
}