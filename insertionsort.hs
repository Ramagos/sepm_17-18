help :: Int -> [Int] -> [Int]
help x [] = [x]
help x (y:ys) = if x < y 
                 then x:y:ys 
         else y : help x ys

insertionSort :: [Int] -> [Int]
insertionSort [x] = [x]
insertionSort (x:xs) = help x (insertionSort xs)

let unsortiert = [2,7,6,1,9,5]

print :: unsortiert -> IO()
do res <- insertionsort (unsortiert)

print :: res -> IO()
