﻿let rec help x = function
  | [] -> [x]
  | y::ys -> if x<=y then x::y::ys
             else y::(help x ys)

let rec insertionSort = function
  | [] -> []
  | x::xs -> help x (insertionSort xs)

let unsortiert = [8;5;7;10;3;0;-2]
let res = unsortiert |> insertionSort 

printfn "%A" unsortiert
printfn "%A" res