import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;

public class View extends JFrame{
	public View() {
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setTitle("Thanks");
		this.setLayout(new BorderLayout());
		this.setSize(600,400);
		this.setResizable(false);

		GameView gameView=new GameView();
		this.add(gameView, BorderLayout.CENTER);

		JPanel controlls=new JPanel();

		JPanel sliderText=new JPanel();
		sliderText.setLayout(new GridLayout(2,1));
		
		JLabel winkelLabel=new JLabel();
		winkelLabel.setText("Winkel: ");
		sliderText.add(winkelLabel);

		JLabel kraftLabel=new JLabel();
		kraftLabel.setText("Kraft: ");
		sliderText.add(kraftLabel);
		

		JPanel slider=new JPanel();
		slider.setLayout(new GridLayout(2,1));

		JSlider winkel=new JSlider();
		winkel.setMinimum(0);
		winkel.setMaximum(100);
		winkel.setMinorTickSpacing(1);
		winkel.setMajorTickSpacing(25);
		winkel.createStandardLabels(1);
		winkel.setPaintTicks(true);
		winkel.setPaintLabels(true);
		slider.add(winkel);

		JSlider kraft=new JSlider();
		kraft.setMinimum(0);
		kraft.setMaximum(100);
		kraft.setMinorTickSpacing(1);
		kraft.setMajorTickSpacing(25);
		kraft.createStandardLabels(1);
		kraft.setPaintTicks(true);
		kraft.setPaintLabels(true);
		slider.add(kraft);

		JPanel sliderPanel=new JPanel();
		sliderPanel.setLayout(new BorderLayout());
		sliderPanel.add(sliderText, BorderLayout.WEST);
		sliderPanel.add(slider, BorderLayout.CENTER);
		
		controlls.add(sliderPanel);

		JButton schuss=new JButton("Schuss");
		controlls.add(schuss);

		this.add(controlls, BorderLayout.SOUTH);

		this.setVisible(true);
	}
}
