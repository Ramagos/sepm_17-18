import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;

import javax.swing.JPanel;

public class GameView extends JPanel{

	public GameView() {
		this.setSize(600,350);
	}

	public void paintComponent(Graphics g) {
		ArrayList<Integer> mapForm=Controller.gameMap.getForm();
		int scale=6;
		
		g.setColor(Color.GREEN);
		for(int i=0;i<mapForm.size();i++) {
			g.fillRect(i*scale, 400-mapForm.get(i), scale, 400);
		}

		Spieler player1 = Controller.player1;

		g.setColor(Color.BLACK);
		g.fillRect(player1.getPosX()*scale-scale, 400-mapForm.get(player1.getPosX())-scale, 3*scale, scale*2);

		Spieler player2 = Controller.player2;
		
		g.setColor(Color.PINK);
		g.fillRect(player2.getPosX()*scale-scale, 400-mapForm.get(player2.getPosX())-scale, 3*scale, scale*2);
		
	}
}