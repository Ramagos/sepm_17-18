import java.io.Serializable;
import java.util.ArrayList;

public class Map implements Serializable{
	ArrayList<Integer> form=new ArrayList<Integer>();
	int wind=0;

	public Map() {
		this.form.add((int)(Math.random()*200)+100);
		for(int i=0;i<100;i++) {
			this.form.add(this.form.get(i)+(int)(Math.random()*10)-5);
			if(this.form.get(i+1)<10)
				this.form.set(i+1, 10);
			else
				if(this.form.get(i+1)>300)
					this.form.set(i+1, this.form.get(i));
		}
	}

	public ArrayList<Integer> getForm() {
		return form;
	}
	public int getWind() {
		return wind;
	}
	public void setForm(int index,int neuesY) {
		this.form.set(index, neuesY);
	}
	public void setWind(int wind) {
		this.wind = wind;
	}

}