import java.io.Serializable;

public class Spieler implements Serializable{
	int freude=0;
	int posX;
	int posY;

	public Spieler(int mapPos) {
		this.posX=mapPos;
		this.posY=Controller.gameMap.getForm().get(posX);
	}

	public int getFreude() {
		return freude;
	}

	public int getPosX() {
		return posX;
	}

	public int getPosY() {
		return posY;
	}

	public void setFreude(int freude) {
		this.freude = freude;
	}

	public void setPosY(int posY) {
		this.posY = posY;
	}
}